# numix-square-icon-theme

A square-ish shaped icon theme from the Numix Project

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/themes-and-icons/numix-square-icon-theme.git
```

